package com.team.cn.enums;

public enum ActivityEnum {
    ETHNIC_0(0, "活动结束"),
    ETHNIC_1(1, "报名中"),
    COLOUR(2, "错误");

    public static ActivityEnum getActivity(int activityStr) {
        for (ActivityEnum activity : ActivityEnum.values()) {
            if (activity.getStatus() == activityStr) {
                return activity;
            }
        }
        return ActivityEnum.COLOUR;
    }

    ActivityEnum(int status, String statusInfo) {
        this.status = status;
        this.statusInfo = statusInfo;
    }

    private int status;

    private String statusInfo;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    @Override
    public String toString() {
        return "ActivityEnum{" +
                "status=" + status +
                ", statusInfo='" + statusInfo + '\'' +
                '}';
    }
}
