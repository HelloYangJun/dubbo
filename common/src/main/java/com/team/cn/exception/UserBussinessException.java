package com.team.cn.exception;


import com.team.cn.enums.IErrorCode;

import java.util.Map;

public class UserBussinessException extends RuntimeException {
    private IErrorCode iErrorCode;

    private String errorCode;

    private String errorMessage;

    private Map<String, Object> errorData;

    public UserBussinessException(IErrorCode iErrorCode) {
        super(iErrorCode.getMessage());
        this.iErrorCode = iErrorCode;
        this.errorCode = iErrorCode.getErrorCode();
        this.errorMessage = iErrorCode.getMessage();
    }

    public UserBussinessException() {
    }
    public UserBussinessException(String message) {
        super(message);
    }

    public IErrorCode getiErrorCode() {
        return iErrorCode;
    }

    public void setiErrorCode(IErrorCode iErrorCode) {
        this.iErrorCode = iErrorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Map<String, Object> getErrorData() {
        return errorData;
    }

    public void setErrorData(Map<String, Object> errorData) {
        this.errorData = errorData;
    }
}
