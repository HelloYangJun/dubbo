package com.team.cn.service;


import com.team.cn.dto.CourceComment;
import com.team.cn.dto.User;
import com.team.cn.vo.UserVo;

import java.util.Map;

public interface CommentService {
    /**
     * 分页查询评论
     *
     * @param pageSize
     * @param pageNo
     * @return
     */
    Map showCommentList(int pageSize, int pageNo);

    /**
     * 发表评论
     * @param userVo
     * @param courceComment
     * @return
     */
    CourceComment comment(UserVo userVo, CourceComment courceComment);

    /**
     * 回复评论
     *
     * @param courceComment
     */
    void replyComment(UserVo userVo,CourceComment courceComment);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    boolean deleteCommentById(Integer... ids);

    /**
     * 点赞评论
     *
     * @param id
     */
    void doSupport(int id);
}
