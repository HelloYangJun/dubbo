package com.team.cn.service;

import java.util.List;


import com.team.cn.utils.ReturnResult;
import com.team.cn.vo.ActivityRecVo;

import java.util.Map;

public interface CourceService {
    /**
     * 课程模糊搜索
     *
     * @param searchStr
     * @param pageNo
     * @param pageSize
     * @return
     */
    Map queryCource(String searchStr, int pageNo, int pageSize);

    /**
     * 活动首页
     *
     * @param pageNo
     * @param pageSize
     * @param city
     * @return
     */
    Map postLoginActivity(int pageNo, int pageSize, int city);

    /**
     * 活动详情
     *
     * @param
     * @return
     */
    List eventDetails(int id);

    /**
     * 已报名
     *
     * @param uid
     * @param activaty
     * @return
     */
    int signedUp(int uid, int activaty);

    /**
     * 取消报名
     *
     * @param uid
     * @param activaty
     * @return
     */
    int cancelEnrollment(int uid, int activaty);

    /**
     * 通过uid查出已经参加的活动
     *
     * @param uid
     * @return
     */
    List queryAttend(String uid);

    List queryAttendBy(int uid, int activity);

    /**
     * 增加活动
     *
     * @param activityRecVo
     * @return
     */
    ReturnResult appActivity(ActivityRecVo activityRecVo);

    String unifiedOrder(int cId, List list) throws Exception;

    //insert进cource_buy账单记录
    boolean add(int cId);

    //修改cource表中的购买数量
    boolean update(int cId);

    boolean updateTrade(String tradeNo);

    boolean deleteTrade(String tradeNo);
}
