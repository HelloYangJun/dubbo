package com.team.cn.service;


import java.util.List;
import java.util.Set;

public interface ShowHomePageService {

    /**
     * 查询导航栏
     * @return
     */
    List queryNavbar();


    /**
     * 查询轮播图
     * @return
     */
    List querySlideShow();

    /**
     * 活动推荐
     * @return
     */
    List queryActivity();

    /**
     * 为你推荐
     * @return
     */
    List queryRecommend();

    /**
     * 课程推荐
     * @return
     */
    List queryCource();

    /**
     * 广告
     * @return
     */
    Set queryAdvertising();


}
