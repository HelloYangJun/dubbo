package com.team.cn.service;

import com.team.cn.utils.ReturnResult;
import com.team.cn.utils.ReturnResultUtils;
import com.team.cn.vo.ActivityRecVo;
import com.team.cn.vo.UserVo;
import org.springframework.validation.annotation.Validated;

public interface UserService {

    public ReturnResult appByTel(UserVo userVo, String authCode);

    public ReturnResult<UserVo> loginByTel(String tel, String authCode, String password, String token);

    public ReturnResult sentAuthCodes(String tel, String type);

    public void exit(String key);
}
