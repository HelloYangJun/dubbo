package com.team.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "用户参与活动情况表")
public class AttendVo implements Serializable {

    @ApiModelProperty(value = "用户参与活动情况表id")
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer uId;

    @ApiModelProperty(value = "活动推荐id")
    private Integer activityId;

}
