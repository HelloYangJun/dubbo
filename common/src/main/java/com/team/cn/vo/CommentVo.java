package com.team.cn.vo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class CommentVo implements Serializable {

    private Integer id;

    private Integer uId;

    private Integer cId;

    private String headImg;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date commentTime;

    private String comment;

    private Integer cLikeNum;

}
