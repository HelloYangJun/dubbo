package com.team.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "课程推荐")
public class CourceVo implements Serializable {

    @ApiModelProperty(value = "课程id")
    private Integer id;

    @ApiModelProperty(value = "课程图片")
    private String cImgUrl;

    @ApiModelProperty(value = "课程名")
    private String courseName;

    @ApiModelProperty(value = "购买数量")
    private Integer buyNum;

    @ApiModelProperty(value = "价格")
    private Double price;

    @ApiModelProperty(value = "课程介绍")
    private String cIntroduce;

}


