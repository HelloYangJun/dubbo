package com.team.cn.conf;

import com.alibaba.fastjson.JSONObject;
import com.team.cn.enums.GmEnum;
import com.team.cn.utils.RedisUtils;
import com.team.cn.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class LoginRedComplet implements HandlerInterceptor {

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        LoginRedis methodAnnotation = method.getAnnotation(LoginRedis.class);
        if (null != methodAnnotation) {
            String token = request.getHeader("token");
            if (!StringUtils.isEmpty(token)) {
                String userToken = (String) redisUtils.get(GmEnum.LOGIN_TOKEN.getStr() + token);
                if (!StringUtils.isEmpty(userToken)) {
                    UserVo userVo = JSONObject.parseObject(userToken, UserVo.class);
                    request.setAttribute("user", userVo);
                } else {
                    throw new RuntimeException("login error");
                }
            } else {
                throw new RuntimeException("login error");
            }
            return true;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}