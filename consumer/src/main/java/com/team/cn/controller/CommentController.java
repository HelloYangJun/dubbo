package com.team.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.team.cn.conf.CurrentUser;
import com.team.cn.conf.LoginRedis;
import com.team.cn.dto.CourceComment;
import com.team.cn.dto.User;
import com.team.cn.exception.UserBussinessException;
import com.team.cn.service.CommentService;
import com.team.cn.utils.PageUtils;
import com.team.cn.utils.ReturnResult;
import com.team.cn.utils.ReturnResultUtils;
import com.team.cn.vo.CommentVo;
import com.team.cn.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/comment")
@Api(tags = "评论")
@Log4j
public class CommentController {

    @Reference
    private CommentService commentService;

    @ApiOperation(value = "评论列表")
    @PostMapping(value = "/showCommentList")
    public ReturnResult<PageUtils<List<CommentVo>>> showCommentList(@RequestParam(name = "pageNo", defaultValue = "1") int pageNo,
                                                                    @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        pageUtils.setPageSize(pageSize);
        Map<String, Object> maps = commentService.showCommentList(pageUtils.getPageNo(), pageSize);
        pageUtils.setCurrentList((List<CommentVo>) maps.get("commentList"));
        pageUtils.setTotalCount((long) maps.get("count"));
        pageUtils.setCurrentPage(pageNo);
        return ReturnResultUtils.returnSuccess(pageUtils);
    }

    @LoginRedis
    @ApiOperation(value = "发表评论")
    @PostMapping("/comment")
    public ReturnResult comment(@CurrentUser UserVo userVo, @Validated CourceComment comment) {
        try {
            commentService.comment(userVo, comment);
        } catch (UserBussinessException e) {
            e.printStackTrace();
            return ReturnResultUtils.returnFail(101, "评论发表失败");
        }
        return ReturnResultUtils.returnSuccess("评论发表成功");
    }

    @LoginRedis
    @ApiOperation(value = "回复评论")
    @PostMapping("/reply")
    public ReturnResult replyComment(@CurrentUser UserVo userVo, @Validated CourceComment comment) {
        try {
            commentService.replyComment(userVo, comment);
        } catch (UserBussinessException e) {
            e.printStackTrace();
            return ReturnResultUtils.returnFail(111, "回复评论失败");
        }
        return ReturnResultUtils.returnSuccess("回复评论成功");
    }

    @ApiOperation(value = "admin删除评论")
    @GetMapping(value = "/delete")
    public ReturnResult deleteCommentById(Integer... ids) {
        if (commentService.deleteCommentById(ids)) {
            return ReturnResultUtils.returnSuccess("成功删除" + ids.length + "条评论");
        }
        return ReturnResultUtils.returnFail(111, "删除失败!");
    }
}
