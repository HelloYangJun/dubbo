package com.team.cn.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.team.cn.conf.LoginRedis;
import com.team.cn.conf.WxPayConfig;
import com.team.cn.service.CourceService;
import com.team.cn.utils.ReturnResult;
import com.team.cn.utils.ReturnResultUtils;
import com.team.cn.utils.WxPayClient;
import com.team.cn.utils.WxPayUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "购买课程")
@RequestMapping(value = "/wxPay")
public class CourceBuyController {

    @Reference
    private CourceService courceService;
    @Autowired
    private WxPayConfig wxPayConfig;

    @LoginRedis
    @GetMapping(value = "/pay")
    @ApiOperation(value = "下单")
    public ReturnResult toPay(@RequestParam(value = "cId") int cId) throws Exception {
        List list = Lists.newArrayList();
        list.add(wxPayConfig.getAppid());
        list.add(wxPayConfig.getMch_id());
        list.add(wxPayConfig.getKey());
        list.add(wxPayConfig.getNotify_url());
        list.add(wxPayConfig.getUnifiedUrl());
        return ReturnResultUtils.returnSuccess(courceService.unifiedOrder(cId, list));
    }

    @RequestMapping(value = "/payNotify")
    @ApiOperation(value = "通知验证提醒")
    public String wxPayNotify(HttpServletRequest request) throws Exception {
        //todo 验证是否付款成功
        Map<String, String> resultMap = WxPayClient.getNotifyParameter(request);
        String trade_no = resultMap.get("out_trade_no");
        //todo 验证签名
        String wxSign = resultMap.get("sign");
        boolean isCheck = WxPayUtils.isCheckSign(resultMap, wxPayConfig.getKey(), wxSign);
        //todo insert进账单
        if (isCheck) {
            courceService.updateTrade(trade_no);
            WxPayUtils.getLogger().info("wxnotify:微信支付----验证签名成功");
            Map<String, String> clientMap = Maps.newHashMap();
            clientMap.put("return_code", "SUCCESS");
            clientMap.put("return_msg", "OK");
            return WxPayUtils.mapToXml(clientMap);
        } else {
            courceService.deleteTrade(trade_no);
            WxPayUtils.getLogger().error("wxnotify:微信支付----判断签名错误");
        }
        return null;
    }
}
