package com.team.cn.utils;

import java.util.UUID;

public class RandomUtils {

    public static String getUUID(){
        return UUID.randomUUID().toString().replaceAll("-","").substring(0,32);
    }
}
