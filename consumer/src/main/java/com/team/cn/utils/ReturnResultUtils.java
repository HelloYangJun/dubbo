package com.team.cn.utils;

public class ReturnResultUtils {
    /**
     * 不带data的成功
     * @return
     */

    public static ReturnResult returnResult(){
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(200);
        returnResult.setMessage("success");
        return returnResult;
    }

    /**
     * 带参数的成功
     * @param data
     * @return
     */
    public static ReturnResult returnSuccess(Object data){
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(200);
        returnResult.setMessage("success");
        returnResult.setData(data);
        return returnResult;
    }

    /**
     * 失败
     * @param code
     * @param message
     * @return
     */
    public static ReturnResult returnFail(int code,String message){
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMessage(message);
        return returnResult;
    }

    public static ReturnResult returnResultDiy(int code,String message,Object data){
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMessage(message);
        returnResult.setData(data);
        return returnResult;
    }
}
