package com.team.cn.wx;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "wx")
public class WxConfig {
    private String appId;
    private String redirectUri;
    private String responseType;
    private String scope;
    private String codeUrl;
    private String accessTokenUri;
    private String secret;
    private String grantType;
    private String userInfoUri;
    private String loginSuccess;

    public String reqCodeUri() {
        StringBuffer stringBuffer = new StringBuffer(getCodeUrl());
        stringBuffer.append("?").append("appid=").append(getAppId());
        stringBuffer.append("&").append("redirect_uri=").append(getRedirectUri());
        stringBuffer.append("&").append("response_type=").append(getResponseType());
        stringBuffer.append("&").append("scope=").append(getScope());
        stringBuffer.append("&").append("state=").append("STATE");
        stringBuffer.append("#wechat_redirect");
        return stringBuffer.toString();
    }

    public String reqAccessTokenUri(String code) {
        StringBuffer sb = new StringBuffer(getAccessTokenUri());
        sb.append("?").append("appid=").append(getAppId());
        sb.append("&").append("secret=").append(getSecret());
        sb.append("&").append("code=").append(code);
        sb.append("&").append("grant_type=").append(getGrantType());
        return sb.toString();
    }

    public String reqUserInfoUri(String accessToken, String openId) {
        StringBuffer sb = new StringBuffer(getUserInfoUri());
        sb.append("?").append("access_token=").append(accessToken);
        sb.append("&").append("openid=").append(openId).append("&lang=zh_CN");
        return sb.toString();
    }

}
