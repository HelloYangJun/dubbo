package com.team.cn.mapper;

import com.team.cn.dto.CourceBuy;
import com.team.cn.dto.CourceBuyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CourceBuyMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    long countByExample(CourceBuyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    int deleteByExample(CourceBuyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    int insert(CourceBuy record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    int insertSelective(CourceBuy record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    List<CourceBuy> selectByExample(CourceBuyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    CourceBuy selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") CourceBuy record, @Param("example") CourceBuyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") CourceBuy record, @Param("example") CourceBuyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(CourceBuy record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource_buy
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(CourceBuy record);
}