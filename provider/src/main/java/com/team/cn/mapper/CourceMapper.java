package com.team.cn.mapper;

import com.team.cn.dto.Cource;
import com.team.cn.dto.CourceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CourceMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    long countByExample(CourceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    int deleteByExample(CourceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    int insert(Cource record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    int insertSelective(Cource record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    List<Cource> selectByExample(CourceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    Cource selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") Cource record, @Param("example") CourceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") Cource record, @Param("example") CourceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(Cource record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cource
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(Cource record);
}