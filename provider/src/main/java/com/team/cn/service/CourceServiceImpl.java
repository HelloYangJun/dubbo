package com.team.cn.service;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.team.cn.dto.*;
import com.team.cn.mapper.*;
import com.team.cn.utils.ReturnResult;
import com.team.cn.utils.ReturnResultUtils;
import com.team.cn.vo.ActivityRecVo;
import com.team.cn.vo.AttendVo;
import com.team.cn.vo.CourceVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import com.alibaba.dubbo.config.annotation.Service;
import com.team.cn.dto.Cource;
import com.team.cn.dto.CourceBuy;
import com.team.cn.dto.User;
import com.team.cn.mapper.CourceMapper;
import com.team.cn.utils.CommonUtils;
import com.team.cn.utils.HttpClientUtils;
import com.team.cn.utils.WxPayUtils;
import org.springframework.beans.factory.annotation.Autowired;


import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

@Service
public class CourceServiceImpl implements CourceService {

    @Autowired
    private CourceMapper courceMapper;
    @Autowired
    private ActivityRecMapper activityRecMapper;
    @Autowired
    private AttendMapper attendMapper;
    @Autowired
    private CourceBuyMapper courceBuyMapper;
    @Autowired
    private TradeMapper tradeMapper;


    @Override
    public Map queryCource(String searchStr, int pageNo, int pageSize) {
        CourceExample courceExample = new CourceExample();
        courceExample.createCriteria().andCourseNameLike("%" + searchStr + "%");
        courceExample.setLimit(pageNo);
        courceExample.setOffset(pageSize);
        Map<String, Object> map = Maps.newHashMap();
        List<Cource> courceList = courceMapper.selectByExample(courceExample);
        long count = courceMapper.countByExample(courceExample);
        List<CourceVo> courceVoList = Lists.newArrayList();
        courceList.forEach(list -> {
            CourceVo courceVo = new CourceVo();
            BeanUtils.copyProperties(list, courceVo);
            courceVoList.add(courceVo);
        });
        map.put("courceVoList", courceVoList);
        map.put("count", count);
        return map;
    }

    @Override
    public Map postLoginActivity(int pageNo, int pageSize, int city) {
        Map<String, Object> pageMap = Maps.newHashMap();
        long newCount = 0;
        long oldCount = 0;
        long count = 0;
        ActivityRecExample activityRecExample = new ActivityRecExample();
        ActivityRecExample.Criteria criteria = activityRecExample.createCriteria();
        if (city == 0) {
            //直接将最新活动和往期活动分页
            pageMap = queryByPage(pageNo, pageSize, activityRecExample, criteria, city);

        } else {
            //按照选择的城市给最新活动和往期活动分页
            criteria.andCityEqualTo(city);
            pageMap = queryByPage(pageNo, pageSize, activityRecExample, criteria, city);
        }
        return pageMap;
    }

    //分页里公共的方法
    public Map queryByPage(int pageNo, int pageSize, ActivityRecExample activityRecExample, ActivityRecExample.Criteria criteria, int city) {
        Map<String, Object> pageMap = Maps.newHashMap();
        long newCount = 0;
        long oldCount = 0;
        long count = 0;
        //ActivityRecExample activityRecExample = new ActivityRecExample();
        criteria.andStatusEqualTo(1);
        activityRecExample.setLimit(pageNo);
        activityRecExample.setOffset(pageSize);
        List<ActivityRec> newActivityRecList = activityRecMapper.selectByExampleWithBLOBs(activityRecExample);
        List<ActivityRecVo> newActivityRecVoList = Lists.newArrayList();
        newCount = activityRecMapper.countByExample(activityRecExample);
        newActivityRecList.stream().forEach(activityRec -> {
            ActivityRecVo activityRecVo = new ActivityRecVo();
            BeanUtils.copyProperties(activityRec, activityRecVo);
            newActivityRecVoList.add(activityRecVo);
        });
        pageMap.put("newActivityRecVoList", newActivityRecVoList);
        activityRecExample.clear();
        ActivityRecExample.Criteria criteria1 = activityRecExample.createCriteria();
        if (city != 0) criteria1.andCityEqualTo(city);
        criteria1.andStatusEqualTo(0);
        activityRecExample.setLimit(pageNo);
        activityRecExample.setOffset(pageSize);
        List<ActivityRec> oldActivityRecList = activityRecMapper.selectByExampleWithBLOBs(activityRecExample);
        List<ActivityRecVo> oldActivityRecVoList = Lists.newArrayList();
        oldCount = activityRecMapper.countByExample(activityRecExample);
        oldActivityRecList.stream().forEach(list -> {
            ActivityRecVo activityRecVo = new ActivityRecVo();
            BeanUtils.copyProperties(list, activityRecVo);
            oldActivityRecVoList.add(activityRecVo);
        });
        pageMap.put("oldActivityRecVoList", oldActivityRecVoList);
        count = newCount > oldCount ? newCount : oldCount;
        pageMap.put("count", count);
        return pageMap;
    }

    @Override
    public List eventDetails(int id) {
        ActivityRecExample activityRecExample = new ActivityRecExample();
        activityRecExample.createCriteria().andIdEqualTo(id);
        List<ActivityRec> activityRecs = activityRecMapper.selectByExampleWithBLOBs(activityRecExample);
        List<ActivityRecVo> activityRecVos = Lists.newArrayList();
        activityRecs.stream().forEach(list -> {
            ActivityRecVo activityRecVo = new ActivityRecVo();
            BeanUtils.copyProperties(list, activityRecVo);
            activityRecVos.add(activityRecVo);
        });
        return activityRecVos;
    }

    @Override
    public List queryAttend(String uid) {
        AttendExample attendExample = new AttendExample();
        if (!StringUtils.isEmpty(uid)) {
            attendExample.createCriteria().andUIdEqualTo(Integer.parseInt(uid));
            List<Attend> attendList = attendMapper.selectByExample(attendExample);
            List<AttendVo> attendVos = Lists.newArrayList();
            attendList.forEach(attend -> {
                AttendVo attendVo = new AttendVo();
                BeanUtils.copyProperties(attend, attendVo);
                attendVos.add(attendVo);
            });
            return attendVos;
        }
        return null;
    }


    //统一下单方法
    @Override
    public String unifiedOrder(int cId, List list) throws Exception {
        Cource cource = courceMapper.selectByPrimaryKey(cId);
        SortedMap<String, String> param = new TreeMap<>();
        //公众账号ID
        param.put("appid", String.valueOf(list.get(0)));
        //商户号
        param.put("mch_id", String.valueOf(list.get(1)));
        //随机字符串
        param.put("nonce_str", CommonUtils.generatorUUID());
        //商品描述
        param.put("body", cource.getCourseName());
        //商户订单号
        param.put("out_trade_no", String.valueOf(System.nanoTime()));
        //订单金额
        String price = String.valueOf(new Double(cource.getPrice()).intValue());
        param.put("total_fee", price);
        //终端ip
        param.put("spbill_create_ip", "192.168.43.103");
        //通知地址
        param.put("notify_url", String.valueOf(list.get(3)));
        //交易类型
        param.put("trade_type", "NATIVE");
        //商品id
        param.put("product_id", String.valueOf(cId));
        //签名
        param.put("sign", WxPayUtils.generateSignature(param, String.valueOf(list.get(2))));
        String payXml = WxPayUtils.mapToXml(param);
        //请求统一下单接口
        String order = HttpClientUtils.doPost(String.valueOf(list.get(4)), payXml, 4000);
        Map<String, String> resultMap = WxPayUtils.xmlToMap(order);
        String returnCode = resultMap.get("return_code");
        String resultCode = resultMap.get("result_code");
        if ("SUCCESS".equals(returnCode) && "SUCCESS".equals(resultCode)) {
            String prepayId = resultMap.get("prepay_id");
            String codeUrl = resultMap.get("code_url");
            Trade trade = new Trade();
            trade.setTradeNo(param.get("out_trade_no"));
            trade.setMchId(param.get("mch_id"));
            trade.setcId(cId);
            trade.setTotalFee(Double.parseDouble(param.get("total_fee")));
            trade.setBody(param.get("body"));
            tradeMapper.insertSelective(trade);
            return codeUrl;
        }
        return null;
    }

    @Override
    public List queryAttendBy(int uid, int activity) {
        AttendExample attendExample = new AttendExample();
        attendExample.createCriteria().andActivityIdEqualTo(activity).andUIdEqualTo(uid);
        List<Attend> attends = attendMapper.selectByExample(attendExample);
        List<AttendVo> attendVoList = Lists.newArrayList();
        attends.stream().forEach(list -> {
            AttendVo attendVo = new AttendVo();
            BeanUtils.copyProperties(list, attendVo);
            attendVoList.add(attendVo);
        });
        return attendVoList;
    }

    @Override
    public int signedUp(int uid, int activaty) {
        Attend attend = new Attend();
        attend.setuId(uid);
        attend.setActivityId(activaty);
        return attendMapper.insertSelective(attend);
    }

    @Override
    public int cancelEnrollment(int uid, int activaty) {
        AttendExample attendExample = new AttendExample();
        attendExample.createCriteria().andUIdEqualTo(uid).andActivityIdEqualTo(activaty);
        return attendMapper.deleteByExample(attendExample);
    }

    public ReturnResult appActivity(ActivityRecVo activityRecVo) {
        ActivityRec activityRec = new ActivityRec();
        BeanUtils.copyProperties(activityRecVo, activityRec);

        if (activityRecMapper.insertSelective(activityRec) == 1) {
            return ReturnResultUtils.returnSuccess("增加成功");
        }
        return ReturnResultUtils.returnFail(413, "操作失败");
    }

    public boolean add(int cId) {
        HttpServletRequest request = null;
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("LoginUser");
        CourceBuy courceBuy = new CourceBuy();
        courceBuy.setCourceId(cId);
        courceBuy.setuId(loginUser.getId());
        return courceBuyMapper.insertSelective(courceBuy) > 0;
    }

    @Override
    public boolean update(int cId) {
        Cource cource = new Cource();
        cource.setBuyNum(cource.getBuyNum() + 1);
        return courceMapper.updateByPrimaryKeySelective(cource) > 0;
    }

    @Override
    public boolean updateTrade(String tradeNo) {
        Trade trade = tradeMapper.selectByPrimaryKey(tradeNo);
        trade.settStatue(1);
        return tradeMapper.updateByPrimaryKey(trade) > 0;
    }

    @Override
    public boolean deleteTrade(String tradeNo) {
        return tradeMapper.deleteByPrimaryKey(tradeNo) > 0;
    }

}
