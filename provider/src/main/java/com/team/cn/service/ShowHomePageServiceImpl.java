package com.team.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.team.cn.dto.*;
import com.team.cn.enums.ActivityEnum;
import com.team.cn.mapper.*;
import com.team.cn.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
import java.util.Set;

@Service
public class ShowHomePageServiceImpl implements ShowHomePageService {

    @Autowired
    private NavbarMapper navbarMapper;
    @Autowired
    private BannerMapper bannerMapper;
    @Autowired
    private ActivityRecMapper activityRecMapper;
    @Autowired
    private MenuDetailsMapper menuDetailsMapper;
    @Autowired
    private CourceMapper courceMapper;
    @Autowired
    private AdverMapper adverMapper;

    //查询导航栏
    @Override
    public List<NavbarVo> queryNavbar() {
        NavbarExample navbarExample = new NavbarExample();
        List<Navbar> navbarList = navbarMapper.selectByExample(navbarExample);
        List<NavbarVo> navbarVoList = Lists.newArrayList();
        navbarList.stream().forEach(list -> {
            NavbarVo navbarVo = new NavbarVo();
            BeanUtils.copyProperties(list, navbarVo);
            navbarVoList.add(navbarVo);
        });
        return navbarVoList;
    }

    //查询轮播图
    @Override
    public List<BannerVo> querySlideShow() {
        BannerExample bannerExample = new BannerExample();
        List<Banner> bannerList = bannerMapper.selectByExample(bannerExample);
        List<BannerVo> bannerVoList = Lists.newArrayList();
        bannerList.stream().forEach(list -> {
            BannerVo bannerVo = new BannerVo();
            BeanUtils.copyProperties(list, bannerVo);
            bannerVoList.add(bannerVo);
        });
        return bannerVoList;
    }

    //活动推荐
    @Override
    public List<ActivityRecVo> queryActivity() {
        ActivityRecExample activityRecExample = new ActivityRecExample();
        List<ActivityRec> activityRecList = activityRecMapper.selectExampleWithBLOBs(activityRecExample);
        List<ActivityRecVo> activityRecVoList = Lists.newArrayList();
        activityRecList.stream().forEach(list -> {
            ActivityRecVo activityRecVo = new ActivityRecVo();
            BeanUtils.copyProperties(list, activityRecVo);
            switch (ActivityEnum.getActivity(list.getStatus())) {
                case ETHNIC_0:
                    activityRecVo.setStatusInfo(ActivityEnum.ETHNIC_0.getStatusInfo());
                    break;
                case ETHNIC_1:
                    activityRecVo.setStatusInfo(ActivityEnum.ETHNIC_1.getStatusInfo());
                    break;
                case COLOUR:
                    activityRecVo.setStatusInfo(ActivityEnum.COLOUR.getStatusInfo());
                    break;
            }
            activityRecVoList.add(activityRecVo);
        });
        return activityRecVoList;
    }

    //为你推荐
    @Override
    public List<MenuDetailsVo> queryRecommend() {
        MenuDetailsExample menuDetailsExample = new MenuDetailsExample();
        List<MenuDetails> menuDetailsList = menuDetailsMapper.selectByExampleWithBLOBs(menuDetailsExample);
        List<MenuDetailsVo> menuDetailsVoList = Lists.newArrayList();
        menuDetailsList.stream().forEach(list -> {
            MenuDetailsVo menuDetailsVo = new MenuDetailsVo();
            BeanUtils.copyProperties(list, menuDetailsVo);
            menuDetailsVoList.add(menuDetailsVo);
        });
        return menuDetailsVoList;
    }

    //课程推荐
    @Override
    public List<CourceVo> queryCource() {
        CourceExample courceExample = new CourceExample();
        List<Cource> courceList = courceMapper.selectByExample(courceExample);
        List<CourceVo> courceVoList = Lists.newArrayList();
        courceList.stream().forEach(list -> {
            CourceVo courceVo = new CourceVo();
            BeanUtils.copyProperties(list, courceVo);
            courceVoList.add(courceVo);
        });
        return courceVoList;
    }

    //广告
    @Override
    public Set<AdverVo> queryAdvertising() {
        AdverExample adverExample = new AdverExample();
        List<Adver> adverList = adverMapper.selectByExample(adverExample);
        Set<AdverVo> adverVoSet = Sets.newHashSet();
        //循环
        while (adverVoSet.size() < 2) {
            int sum = 0;
            int randon = (int) Math.ceil(Math.random() * 100);
            //遍历4  6  7
            for (int i = 0; i < adverList.size(); i++) {
                //判断是否在这个区间，如果不是继续遍历
                if (sum < randon && randon <= adverList.get(i).getPrNum() + sum) {
                    //如果有的话把这个记录塞到到set里面
                    AdverVo adverVo = new AdverVo();
                    BeanUtils.copyProperties(adverList.get(i), adverVo);
                    adverVoSet.add(adverVo);
                }
                sum += adverList.get(i).getPrNum();
            }
        }
        return adverVoSet;
    }
}
