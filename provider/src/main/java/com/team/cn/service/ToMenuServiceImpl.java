package com.team.cn.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.team.cn.dto.*;
import com.team.cn.enums.UserErrorEnums;
import com.team.cn.exception.UserBussinessException;
import com.team.cn.mapper.LikingMapper;
import com.team.cn.mapper.MenuDetailsMapper;
import com.team.cn.utils.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ToMenuServiceImpl implements ToMenuService {

    @Autowired
    private MenuDetailsMapper menuDetailsMapper;
    @Autowired
    private LikingMapper likingMapper;
    @Autowired
    private RedisUtils redisUtils;

    String like_key = "Like_Key:";
    String sum_like_key = "Sum_Like_Key:";

    @Override
    public List<MenuDetailsDto> queryByMid(int mId) {
        MenuDetailsExample menuDetailsExample = new MenuDetailsExample();
        menuDetailsExample.createCriteria().andMIdEqualTo(mId);
        List<MenuDetails> menuDetailsList = menuDetailsMapper.selectByExampleWithBLOBs(menuDetailsExample);
        List<MenuDetailsDto> menuDetailsDtoList = Lists.newArrayList();
        menuDetailsList.forEach(menuDetails -> {
            MenuDetailsDto menuDetailsDto = new MenuDetailsDto();
            BeanUtils.copyProperties(menuDetails, menuDetailsDto);
            menuDetailsDtoList.add(menuDetailsDto);
        });
        return menuDetailsDtoList;
    }

    @Override
    public List queryLike(String uId, int mId) {
        LikingExample likingExample = new LikingExample();
        likingExample.createCriteria().andMIdEqualTo(mId).andUIdEqualTo(Integer.valueOf(uId));
        List<Liking> likeList = likingMapper.selectByExample(likingExample);
        List<Integer> list = likeList.stream().map(liking -> liking.getaId()).collect(Collectors.toList());
        return list;
    }


    public boolean isLike(String uId, String aId) {
        boolean result = false;
        List<Object> redisVar = redisUtils.lGet(like_key + uId + ":" + aId, 0, -1);
        List<Integer> integerList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(redisVar)) {
            redisVar.forEach(r -> {
                integerList.add(Integer.valueOf(String.valueOf(r)));
            });
            if (integerList.stream().mapToInt(v -> v).sum() > 0) result = true;
        } else {
            //网络波动的情况从数据库查
            LikingExample likingExample = new LikingExample();
            likingExample.createCriteria().andUIdEqualTo(Integer.parseInt(uId)).andAIdEqualTo(Integer.parseInt(aId));
            List<Liking> likingList = likingMapper.selectByExample(likingExample);
            if (CollectionUtils.isNotEmpty(likingList)) result = true;
        }
        return result;
    }

    @Override
    public void like(String uId, String aId, int num) throws UserBussinessException {
        if (isLike(uId, aId) && num == +1) {
            throw new UserBussinessException(UserErrorEnums.ALREADY_LIKE);
        }
        redisUtils.lSet(like_key + uId + ":" + aId, num);
        if (num == +1) {
            redisUtils.incr(sum_like_key + aId, 1);
        } else {
            redisUtils.decr(sum_like_key + aId, 1);
        }
    }

    @Async
    @Override
    public void saveInDB(String uId, String aId,String mId) {
        List<Object> redisVar = redisUtils.lGet(like_key + uId + ":" + aId, 0, -1);
        List<Integer> integerList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(redisVar)) {
            redisVar.forEach(r -> {
                integerList.add(Integer.valueOf(String.valueOf(r)));
            });
            Liking liking = new Liking();
            if (integerList.stream().mapToInt(v -> v).sum() > 0) {
                liking.setuId(Integer.parseInt(uId));
                liking.setaId(Integer.parseInt(aId));
                liking.setmId(Integer.parseInt(mId));
                likingMapper.insertSelective(liking);
            }else{
                LikingExample likingExample = new LikingExample();
                likingExample.createCriteria().andUIdEqualTo(Integer.parseInt(uId)).andAIdEqualTo(Integer.parseInt(aId));
                likingMapper.deleteByExample(likingExample);
            }
        }

    }
}
