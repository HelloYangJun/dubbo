package com.team.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.team.cn.dto.ActivityRec;
import com.team.cn.dto.User;
import com.team.cn.dto.UserExample;
import com.team.cn.enums.GmEnum;
import com.team.cn.mapper.ActivityRecMapper;
import com.team.cn.mapper.UserMapper;
import com.team.cn.utils.*;
import com.team.cn.vo.ActivityRecVo;
import com.team.cn.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.swing.*;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private ReturnResultUtils returnResultUtils;

    @Override
    public ReturnResult appByTel(UserVo userVo, String authCode) {
        String num = redisUtils.get(GmEnum.TEL_LOGIN.getStr() + userVo.getPhone()).toString();
        if (authCode.equals(num)) {
            UserExample userExampl = new UserExample();
            userExampl.createCriteria().andPhoneEqualTo(userVo.getPhone());
            List<User> list = userMapper.selectByExample(userExampl);
            //判断是否有改用户
            if (ObjectUtils.isEmpty(list)) {
                User user = new User();
                user.setPhone(userVo.getPhone());
                user.setUserName(userVo.getUserName());
                user.setPassword(SHAUtils.stringSha1(userVo.getPassword()));
                if (userMapper.insert(user) == 1) {
                    return returnResultUtils.returnSuccess("注册成功");
                }
                return returnResultUtils.returnFail(409, "注册失败");
            } else {
                return returnResultUtils.returnFail(402, "电话号码已注册");
            }
        }
        return returnResultUtils.returnFail(401, "验证码错误");
    }


    @Override
    public ReturnResult<UserVo> loginByTel(String tel, String authCode, String password, String token) {
        UserExample userExampl = new UserExample();
        userExampl.createCriteria().andPhoneEqualTo(tel);
        List<User> list = userMapper.selectByExample(userExampl);
        if (ObjectUtils.isEmpty(list)) {
            return returnResultUtils.returnFail(403, "不存在改用户");
        } else {
            if (StringUtils.isEmpty(authCode)) {
                if (!SHAUtils.stringSha1(password).equals(list.get(0).getPassword()))
                    return returnResultUtils.returnFail(411, "密码输入错误");
            } else {
                String num = redisUtils.get(GmEnum.TEL_REGISTER.getStr()+ tel).toString();
                if (!authCode.equals(num)) {
                    return returnResultUtils.returnFail(401, "验证码不正确");
                }
            }
            User user = list.get(0);
            UserVo userVo = new UserVo();
            BeanUtils.copyProperties(user, userVo);
            userVo.setToken(token);
            String jsonstr = JSON.toJSONString(userVo);
            redisUtils.set(GmEnum.LOGIN_TOKEN.getStr()+token, jsonstr);
            redisUtils.expire(GmEnum.LOGIN_TOKEN.getStr()+token, 1000);
            return returnResultUtils.returnResultDiy(201, "登陆成功", userVo);
        }
    }


    @Override
    public ReturnResult sentAuthCodes(String tel, String type) {
        //      String phoneNumber = request.getParameter("phoneNumber");//获取前端传送过来的电话号码
        String randomNum = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 6);//随机生成6位数的验证码
        String jsonContent = "{\"code\":\"" + randomNum + "\"}";//这里的code对应在阿里大于创建短信模板时的${code}
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("phoneNumber", tel);
        //签名和模板是我自己创建的
        paramMap.put("msgSign", "我的家乡");//阿里大于创建短信签名的签名名称
        paramMap.put("templateCode", "SMS_184105274");//阿里大于创建短信模板的模版名称
        paramMap.put("jsonContent", jsonContent);
        SendSmsResponse sendSmsResponse = SendMessageUtils.sendSms(paramMap); //阿里大于发送机制
        if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
            switch (type) {
                case "1":
                    //手机号码注册发送验证码
                    redisUtils.set(GmEnum.TEL_LOGIN.getStr() + tel, randomNum);
                    redisUtils.expire(GmEnum.TEL_LOGIN.getStr()+ tel, 1000);
                    return returnResultUtils.returnSuccess("发送验证成功");
                case "2":
                    //手机号码登陆发送验证码
                    redisUtils.set(GmEnum.TEL_REGISTER.getStr()+ tel, randomNum);
                    redisUtils.expire(GmEnum.TEL_REGISTER.getStr() + tel, 1000);
                    return returnResultUtils.returnSuccess("发送验证成功");
                case "3":
                    //手机号码绑定微信发送验证码
                    redisUtils.set(GmEnum.TEL_BINDING.getStr()+ tel, randomNum);
                    redisUtils.expire(GmEnum.TEL_BINDING.getStr()+ tel, 1000);
                    return returnResultUtils.returnSuccess("发送验证成功");
            }
        }
        return returnResultUtils.returnFail(404, "发送验证失败");
    }

    @Override
    public void exit(String key) {
        redisUtils.del(GmEnum.LOGIN_TOKEN.getStr()+key);
    }




}
