package com.team.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.team.cn.dto.*;
import com.team.cn.enums.GmEnum;
import com.team.cn.mapper.UserMapper;
import com.team.cn.mapper.WxMapper;
import com.team.cn.mapper.WxPhoneMapper;
import com.team.cn.utils.RedisUtils;
import com.team.cn.utils.ReturnResult;
import com.team.cn.utils.ReturnResultUtils;
import com.team.cn.vo.UserVo;
import com.team.cn.vo.WxVo;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import java.util.*;

@Service
public class WxServiceImpl implements WxService {

    @Autowired
    private WxMapper wxMapper;
    @Autowired
    private ReturnResultUtils returnResultUtils;
    @Autowired
    private WxPhoneMapper wxPhoneMapper;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private UserMapper userMapper;

    /*
    用户登陆
     */
    @Override
    public ReturnResult add(JSONObject jsonObject) {
        Wx wx = new Wx();
        WxExample wxExample = new WxExample();
        wxExample.createCriteria().andOpenIdEqualTo(jsonObject.getString("openid"));
        wx.setOpenId(jsonObject.getString("openid"));
        wx.setNickName(jsonObject.getString("nickname"));
        wx.setSex(jsonObject.getInteger("sex"));
        List<Wx> list = wxMapper.selectByExample(wxExample);
        //是否有该用户存在，不存在，可以将注册，存在，报错
        if (ObjectUtils.isEmpty(list)) {
            if (wxMapper.insertSelective(wx) > 0) {
                return returnResultUtils.returnSuccess("注册成功");
            }
            return returnResultUtils.returnFail(403, "注册失败");
        }
        return returnResultUtils.returnFail(402, "用户存在无法注册");
    }

    /*
    登陆
    判断数据库是否有该用户
    判断用户是否有绑定手机号码
    如果没有绑定
    需要进行绑定
    json
     */
    @Override
    public ReturnResult<WxVo> queryWx( WxVo wxVo, String token) {
        WxExample wxExample = new WxExample();
        wxExample.createCriteria().andOpenIdEqualTo(wxVo.getOpenId())
                .andNickNameEqualTo(wxVo.getNickName())
                .andSexEqualTo(wxVo.getSex());
        List<Wx> list = wxMapper.selectByExample(wxExample);
        if (ObjectUtils.isEmpty(list)) {
            return returnResultUtils.returnFail(401, "微信未注册请先注册");
        } else {
            WxPhoneExample wxPhoneExample = new WxPhoneExample();
            wxPhoneExample.createCriteria().andWIdEqualTo(list.get(0).getWxId());
            List<WxPhone> list1 = wxPhoneMapper.selectByExample(wxPhoneExample);
            if (ObjectUtils.isEmpty(list1)) {
                return returnResultUtils.returnFail(405, "微信未绑定手机，请先绑定手机在登陆");
            } else {
                //返回登陆成功
                Wx wx = list.get(0);
                WxVo wxVos = new WxVo();
                BeanUtils.copyProperties(wx, wxVos);
                wxVo.setToken(token);
                String jsonstr = JSON.toJSONString(wxVo);
                redisUtils.set(GmEnum.LOGIN_TOKEN.getStr() + token, jsonstr);
                redisUtils.expire(GmEnum.LOGIN_TOKEN.getStr() + token, 1000);
                return returnResultUtils.returnResultDiy(201, "登陆成功", wxVo);
            }
        }
    }

    /*
    微信信息手机号码进行关联
     */
    @Override
    public ReturnResult wxAndPhone(String tel, String authCode,  WxVo wxVo) {
        WxExample wxExample = new WxExample();
        wxExample.createCriteria().andOpenIdEqualTo(wxVo.getOpenId())
                .andNickNameEqualTo(wxVo.getNickName())
                .andSexEqualTo(wxVo.getSex());
        List<Wx> listWx = wxMapper.selectByExample(wxExample);
        //验证验证码的正确
        String codeDemo = (String) redisUtils.get(GmEnum.TEL_BINDING.getStr() + tel);
        if (authCode.equals(codeDemo)) {
            // 先查询是否有这个电话的用户，有的花进行合并，没有的进行注册，在进行合并
            UserExample userExample = new UserExample();
            userExample.createCriteria().andPhoneEqualTo(tel);
            List<User> list = userMapper.selectByExample(userExample);
            if (ObjectUtils.isEmpty(list)) {
                User user = new User();
                user.setPhone(tel);
                userMapper.insert(user);
                list = userMapper.selectByExample(userExample);
                WxPhone wxPhone = new WxPhone();
                wxPhone.setpId(list.get(0).getId());
                wxPhone.setwId(listWx.get(0).getWxId());
                wxPhoneMapper.insertSelective(wxPhone);
                return returnResultUtils.returnSuccess("手机号码注册并关联成功");
            }
            WxPhoneExample wxPhoneExample = new WxPhoneExample();
            wxPhoneExample.createCriteria().andPIdEqualTo(list.get(0).getId());
            List<WxPhone> list1 = wxPhoneMapper.selectByExample(wxPhoneExample);
            if (ObjectUtils.isEmpty(list1)) {
                WxPhone wxPhone = new WxPhone();
                wxPhone.setpId(list.get(0).getId());
                wxPhone.setwId(listWx.get(0).getWxId());
                wxPhoneMapper.insertSelective(wxPhone);
                return returnResultUtils.returnFail(402, "关联成功");
            }
            return returnResultUtils.returnFail(407, "该手机已经关联过微信，无法在再关联");
        }
        return returnResultUtils.returnFail(406, "验证码错误");
    }

}
